package spittr.web;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeController {

    /*@RequestMapping(value = "home.do", method = RequestMethod.GET)
    public String home(Model model) {
        return "home";
    }*/

    @RequestMapping(method = RequestMethod.GET)
    public String test(Model model) {
        return "test";
    }
}
